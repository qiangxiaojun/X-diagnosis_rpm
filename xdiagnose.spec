%global debug_package %{nil}

Name:		xdiagnose
Version:	1.0.1
Release:	1
Summary:	system diagnostic tool set

License:	MulanPSL-2.0
URL:		https://gitee.com/openeuler/X-diagnosis
Source0:	%{name}-%{version}.tar.gz
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-buildroot

BuildRequires:  python3 python3-setuptools
BuildRequires:	elfutils-devel clang llvm libbpf libbpf-devel libbpf-static

%description
Xdiagnose is an OS troubleshooting tool that integrates functions such as analysis, network tracking, periodic information recording, and historical experience curing.

%prep
%setup -n %{name}-%{version}

%build
python3 setup.py build
sh xdiag_ebpf/build.sh -b

%install
python3 setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
sh xdiag_ebpf/build.sh -i %{buildroot}/%{_bindir}
mkdir -p %{buildroot}%{_sysconfdir}/x-diagnose
cp -a diag.conf %{buildroot}%{_sysconfdir}/x-diagnose
cp -a xdiag_sh/sysinspect/sysinspect %{buildroot}%{_bindir}/xd_sysinspect

%clean
rm -rf $RPM_BUILD_ROOT


%files -f INSTALLED_FILES
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/x-diagnose/diag.conf
%{_bindir}/*


%changelog
* Fri Dec 2 2022 lankstra<lankstra@163.com> - 1.0.1-1
- init version.
